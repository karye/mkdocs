---
description: Bra resurser för att träna C#-programmering på egen hand
---

# Klasser

## Allmänna bra länkar
* [C# referens: Klasser](https://csharp.progdocs.se/klasser-och-objektorientering/klasser-och-instanser)

## Exempel

Skapa en klass som heter `Person` som har en `string`-egenskap som heter `name` och `age`. Skapa sedan en instans av klassen och skriv ut dess namn.

I C# skapas en klass genom att använda class-nyckelordet följt av klassnamnet. Här är ett exempel på hur man kan skapa en klass som heter "Person" med variablerna "name" och "age" samt en konstruktor som sätter dessa värden:

Så här kan du skapa en instans av klassen Person:
    
```csharp
class Person
{
    public string name;
    public int age;
}
```

```mermaid
%%{init: {'theme': 'base', 'themeVariables': 
{ 'primaryBorderColor': 'black', 
'primaryColor': 'white'
}}}%%
classDiagram
    class Person {
        +name : string
        +age : int
    }
```

För att skapa en instans av klassen använder man new-operatorn följt av klassnamnet och konstruktorns parametrar:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Person person = new Person();
        person.name = "Pelle";
        person.age = 10;
    }
}
```

För att skapa en metod i en klass använder man samma syntax som för att skapa en vanlig metod, med undantag för att metoden deklareras inom klammerparantetrar {} för klassen. Här är ett exempel på en metod som skriver ut en person i form av "name (age years old)":

```csharp
class Person
{
    // Variabler
    public string name;
    public int age;
    
    // Metod
    public void PrintPerson()
    {
        Console.WriteLine($"{name} ({age} years old)");
    }
}
```

```mermaid
classDiagram
    class Person {
        +name : string
        +age : int
        +PrintPerson() : void
    }
```

Metoden kan sedan användas på en instans av klassen genom att använda punktnotation för att komma åt metoden:

```csharp
Person person = new Person("Pelle", 10);
person.PrintPerson(); // Skriver ut "Pelle (10 years old)"
```

Med dessa grundläggande kunskaper i åtanke kan man börja implementera de olika uppgifterna i övningarna. För att skapa en meny kan man använda en `while`-loop som körs tills användaren väljer att avsluta programmet. Inuti `while`-loopen kan man skriva ut menyvalen och låta användaren mata in ett val, som sedan används i en `switch`-sats för att kalla olika metoder beroende på vilket val användaren gjorde.

Här är ett exempel på hur detta kan implementeras för den första övningen:

```csharp
// Skapa en lista av personer
List<Person> persons = new List<Person>();
```	

## Övningar

Skapa en ny mapp **RepKlasser**.

### 1. Skapa klass

* Skapa ett nytt konsolprojekt **Klasser1**.
* Skapa en klass som heter `Car`.  
  * Klassen ska ha två parametrar: `brand` och `model`.  
  * Skapa en meny som skriver ut en meny med följande val:  

```shell
1. Add person  
2. List persons  
3. Exit  
Choose:   
```

* Skapa en `List<Car>` som heter `cars` utanför programloopen.  
  * Om användaren väljer "1" så ska användaren kunna mata in namn och ålder på en person.  
  * Om användaren väljer "2" så ska användaren kunna se en lista på alla personer som har lagts till.  
  * Om användaren väljer "3" så ska programmet avslutas.  

### 2. Skapa klass

* Skapa ett nytt konsolprojekt **Klasser2**.
* Skapa en klass som heter `Book`.  
  * Klassen ska ha två parametrar: `title` och `author`.  
  * Skapa en programloop med en meny som skriver ut följande val:  

```shell
1. Add book  
2. Remove book  
3. List books  
4. Exit  
Choose:   
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.  
  * Om användaren väljer "1" så ska användaren kunna mata in titel och författare på en bok.  
  * Om användaren väljer "2" så ska användaren kunna ta bort en bok.  
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla böcker som har lagts till.  
  * Om användaren väljer "4" så ska programmet avslutas.  

### 3. Klasser med klassmetoder

* Skapa ett nytt konsolprojekt **KlasserMetoder**.
* Skapa en klass som heter `Student`.  
  * Klassen ska ha tre parametrar: `name`, `age` och `grade`.  
  * Skapa en programloop med en meny som skriver ut följande val:  

```shell
1. Add student
2. Remove student
3. List students
4. Update student grade
5. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.  
  * Om användaren väljer "1" så ska användaren kunna mata in namn, ålder och betyg på en student.  
  * Om användaren väljer "2" så ska användaren kunna ta bort en student.  
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla studenter som har lagts till.  
  * Om användaren väljer "4" så ska användaren kunna uppdatera betyget på en student.  
  * Om användaren väljer "5" så ska programmet avslutas.  

### 4. Klasser med egenskaper

Exempel på en klass med egenskaper:

```csharp
class Person
{
    // Egenskaper
    public string Name { get; set; }
    public int Age { get; set; }

    // Konstruktor
    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }

    // Metod
    public virtual void PrintPerson()
    {
        Console.WriteLine($"{Name} ({Age} years old)");
    }

    // Return age in dog years
    public int GetAgeInDogYears()
    {
        return Age * 7;
    }
}
```

``` mermaid
%%{init: {'theme': 'base', 'themeVariables': 
{ 'primaryBorderColor': 'black', 
'primaryColor': 'white'
}}}%%
classDiagram
    class Person{
        +Name : string
        +Age : int
        +Person(string name, int age)
        +PrintPerson() void
        +GetAgeInDogYears() int
    }
```

* Skapa ett nytt konsolprojekt **KlasserEgenskaper1**.
* Skapa en klass som heter `Dog`. 
  * Klassen ska ha två parametrar: `name` och `age`.
  * Lägg till egenskaper (properties) för `name` och `age` i klassen.
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add dog
2. Remove dog
3. List dogs
4. Update dog age
5. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna mata in namn och ålder på en hund.
  * Om användaren väljer "2" så ska användaren kunna ta bort en hund.
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla hundar som har lagts till.
  * Om användaren väljer "4" så ska användaren kunna uppdatera åldern på en hund.
  * Om användaren väljer "5" så ska programmet avslutas.

### 5. Klasser med konstruktor

* Skapa ett nytt konsolprojekt **KlasserKonstruktor**.
* Skapa en klass som heter `Circle`. Klassen ska ha en parameter `radius`.
  * Lägg till en metod i klassen som heter `CalculateArea()` som räknar ut och returnerar cirkelns area (π * radius^2).
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add circle
2. List circles
3. Calculate total area
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna mata in radien för en cirkel.
  * Om användaren väljer "2" så ska användaren kunna se en lista på alla cirklar som har lagts till.
  * Om användaren väljer "3" så ska programmet använda metoden `CalculateArea()` för varje cirkel i listan och räkna ut och skriva ut den totala arean av alla cirklar.
  * Om användaren väljer "4" så ska programmet avslutas.

### 6. Klasser med statiska metoder

* Skapa ett nytt konsolprojekt **KlasserStatiskaMetoder**.
* Skapa en klass som heter `Shape` med en konstruktor som tar emot en sträng för formen (t.ex. `triangle`, `square`, etc.).
  * Lägg till en metod i klassen som räknar ut formens omkrets. Denna metod ska returnera en sträng med formens omkrets i form av "The circumference of the [shape] is [omkrets]".
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add shape
2. Calculate shape circumference
3. List shapes
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna mata in formen på en figur.
  * Om användaren väljer "2" så ska användaren kunna räkna ut omkretsen på en figur.
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla figurer som har lagts till.
  * Om användaren väljer "4" så ska programmet avslutas.

### 7. Klasser med egenskaper

* Skapa ett nytt konsolprojekt **KlasserEgenskaper2**.
* Skapa en klass som heter `Employee` med fyra egenskaper: `FirstName`, `LastName`, `Age` och `Salary`.
  * Lägg till en metod i klassen som räknar ut lönen för en anställd efter skatt. Denna metod ska returnera en sträng med formen "[firstName] [lastName]'s salary after tax is [salary]".
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add employee
2. Calculate salary after tax
3. List employees
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna mata in för- och efternamn, ålder och lön för en anställd.
  * Om användaren väljer "2" så ska användaren kunna räkna ut lönen efter skatt för en anställd.
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla anställda som har lagts till.
  * Om användaren väljer "4" så ska programmet avslutas.

### 8. Klasser med arv

* Skapa ett nytt konsolprojekt **KlasserArv**.
* Skapa en klass som heter `Shape`. Klassen ska ha en parameter `color`.
* Skapa en klass som heter `Rectangle` som ärver från klassen `Shape`. Klassen ska ha två parametrar: `width` och `height`.
  * Lägg till en metod i klassen `Rectangle` som heter `CalculateArea()` som räknar ut och returnerar rektangelns area (width * height).
* Skapa en klass som heter `Triangle` som ärver från klassen `Shape`. Klassen ska ha tre parametrar: `side1`, `side2` och `side3`.
  * Lägg till en metod i klassen `Triangle` som heter `CalculateArea()` som räknar ut och returnerar triangelns area (b * h / 2, där b är den kortaste sidan och h är höjden från den längsta sidan).
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add shape
2. List shapes
3. Calculate total area
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna välja mellan att lägga till en rektangel eller en triangel, samt mata in dess parametrar och färg.
  * Om användaren väljer "2" så ska användaren kunna se en lista på alla figurer som har lagts till, inklusive form, färg och dess dimensioner.
  * Om användaren väljer "3" så ska användaren kunna se den totala ytan av alla figurer som har lagts till.
  * Om användaren väljer "4" så ska programmet avslutas.

### 9. Klasser med interface

* Skapa ett nytt konsolprojekt **KlasserInterface**.
* Skapa ett interface som heter `IPrintable` med en metod `Print()` som returnerar en sträng.
  * Skapa en klass som heter `Book` som implementerar interfacen `IPrintable`. Lägg till egenskaper för `title` och `author` i klassen. Implementera metoden "Print()" så att den returnerar en sträng med formen "[title] by [author]".
  * Skapa en klass som heter `Employee` som implementerar interfacen `IPrintable`. Lägg till egenskaper för `firstName`, `lastName` och `salary` i klassen. Implementera metoden `Print()` så att den returnerar en sträng med formen "[firstName] [lastName] - [salary]".
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add printable object
2. Print object
3. List objects
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna välja mellan att lägga till en bok eller en anställd, samt mata in dess parametrar.
  * Om användaren väljer "2" så ska användaren kunna välja ett objekt och skriva ut dess information med hjälp av metoden `Print()`.
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla objekt som har lagts till, inklusive dess typ och information.
  * Om användaren väljer "4" så ska programmet avslutas.

### 10. Klasser med indexerare

* Skapa ett nytt konsolprojekt **KlasserIndexerare**.
* Skapa en klass som heter `Phonebook` med en tom lista för kontakter.
  * Lägg till en indexerare i klassen `Phonebook` som tar emot ett namn och returnerar kontaktens nummer, om kontakten finns i listan. Annars ska indexeraren returnera en sträng med texten "Contact not found".
  * Skapa en programloop med en meny som skriver ut följande val:

```shell
1. Add contact
2. Look up contact by name
3. List all contacts
4. Exit
Choose: 
```

* Skapa en switch-sats som kallar olika metoder beroende på vilket val användaren gjorde.
  * Om användaren väljer "1" så ska användaren kunna mata in namn och nummer för en kontakt.
  * Om användaren väljer "2" så ska användaren kunna mata in ett namn och få tillbaka dess nummer.
  * Om användaren väljer "3" så ska användaren kunna se en lista på alla kontakter som har lagts till, inklusive namn och nummer.
  * Om användaren väljer "4" så ska programmet avslutas.