---
description: Träna på att skapa gui-appar med WPF
---

# Uppgifter

## Uppgift 7

* Skapa en fönsterapp som tar emot ett meddelande på svenska och skriver ut det i morsekod.

![](/.gitbook/assets/image-29%20copy.png)

Konsolekod från programmering 1:

```csharp
Console.WriteLine("Detta program översätter din text till morse.");

// Läs in texten
Console.Write("Ange en text att översätta: ");
string textSvenska = Console.ReadLine();

// Två parallella samlingar
string alfabetet = "ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ ";
string[] morse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", 
                  "....", "..", ".---", "-.-", ".-..", "--", "-.", 
                  "---", ".--.", "--.-", ".-.", "...", "-", ".--", 
                  "...-", ".--", "-..-", "-.--", "--..", ".--.-", 
                  ".-.-", "---.", " "};

// Loopa igenom texten bokstav-för-bokstav
string textPåMorse = "";
foreach (var tecken in textSvenska)
{
    // Plocka ut bokstaven som söks
    string bokstav = tecken.ToString().ToUpper();

    // Hitta index i alfabetet (-1 betyder hittades inte)
    int index = alfabetet.IndexOf(bokstav);
    if (index != -1)
    {
        // Sätt samman meddelandet
        textPåMorse += morse[index];
    }
}

Console.WriteLine($"Din text på morse blir: {textPåMorse}");
```

## Uppgift 8

* Skapa en fönsterapp som är en gästbok.
* Användaren anger sitt namn och en text som han sparar.
* Alla inlägg i gästboken kan man läsa i en större textruta.
* Inläggen sparas automatiskt i en textfil.

![](/.gitbook/assets/image-30%20copy.png)

Konsolekod från programmering 1:

```csharp
Console.WriteLine("Välkommen till gästboken");

// Gästboksfilen
string filen = "./guestbook.txt";

// Läs in gästboksfilen
string allText = "";
if (File.Exists(filen))
{
    allText = File.ReadAllText(filen);
}
else
{
    Console.WriteLine($"{filen} finns inte, avbryter...");
}

// Be om ett inlägg
Console.WriteLine("Skriv inlägg: ");
string inlägg = Console.ReadLine();

// Lägg till inlägget
allText = "\n" + inlägg;

// Spara in gästboksfilen
File.WriteAllText(filen, allText);

// Respons till användaren
Console.WriteLine("Ditt inlägg har sparats");
```

## Uppgift 9

Fortsättning på gästboken:

* När man startar appen laddas alla inlägg in.
* Inlägg visas med senast överst och med datum och namn under varje inlägg.

![](/.gitbook/assets/image-31%20copy.png)

För datum se [Datetime.Now](https://zetcode.com/csharp/datetime/).
