---
description: Hur man får kontroll på layouten
---

# Layout med StackPanel

## Meddelandeappen

Vi bygger en app som skickar meddelanden.

* Vi använder `StackPanel` för att bygga layouten.  
* Vi använder `Button` för att skicka meddelandet.  
* Vi använder `TextBox` för att skriva in epostadressen.
* Vi använder `TextBlock` för att skriva in meddelandet.
* Vi använder `Label` för statusraden.

![](/.gitbook/assets/image-44%20copy.png)

### Grundkoden

I konsolen kör:

```powershell
dotnet new wpf
dotnet run
```

### Skapa en layout

#### MainWindow.xaml

Vi ändrar storleken på fönstret och bakgrundsfärgen:

```xml
<Window ...
        Title="Min första app" Height="300" Width="500"
        Background="#EEE">
```

Börja med att ta bort `Grid`.  
För att bygga en layout kan man använda `StackPanel` för att stapla element:

```xml
<Window ...>
    <StackPanel>
        <Label>Meddelanden</Label>
        <Label>Ange epostadress</Label>
        <TextBox />
        <Label>Ange texten</Label>
        <TextBox />
        <Button>Skicka!</Button>
    </StackPanel>
</Window>
```

![](/.gitbook/assets/image-37%20copy.png)

#### Snygga till layouten

Vi har redan använt attributet `Background` för att byta bakgrundsfärg.

Vi snyggar till layouten med följande attribut:

* `Width` och `Height`
* `Margin`
* `FontSize`

```xml
<Label Margin="5" FontSize="24">Meddelanden</Label>
<Label Margin="5">Ange epostadress</Label>
<TextBox Margin="5" />
<Label Margin="5">Ange texten</Label>
<TextBox Margin="5" />
<Button Margin="5" Height="30">Skicka!</Button>
```

![](/.gitbook/assets/image-38%20copy.png)

#### Stapla horisontellt

Vi vill att `Label` och `TextBox` ska stå bredvid varandra. Det gör vi med `Orientation="Horizontal"`:

Som i HTML där man bygger med div-i-div, kan man bygga en layout med flera `StackPanel` med attribut `Orientation`:

* `HorizontalAlignment`
* `VerticalAlignment`

```xml
<StackPanel Orientation="Horizontal">
    <Label Margin="5">Ange epostadress</Label>
    <TextBox Margin="5" Width="300" />
</StackPanel>
```

![](/.gitbook/assets/image-39%20copy.png)

```xml
<StackPanel Orientation="Horizontal">
    <Label Margin="5">Ange epostadress</Label>
    <TextBox Margin="5" Width="300" />
</StackPanel>
<StackPanel Orientation="Horizontal">
    <Label Margin="5">Ange texten</Label>
    <TextBox Margin="5" Width="300" />
</StackPanel>
```

![](/.gitbook/assets/image-40%20copy.png)

Man behöver justera längden på `Label` för att snygga till formuläret:

```xml
..
    <Label Margin="5" Width="100">Ange epostadress</Label>
..
    <Label Margin="5" Width="100">Ange texten</Label>
..
```

![](/.gitbook/assets/image-41%20copy.png)

För att skriva in ett längre meddelande ökar vi på `Height` och `TextWrapping="Wrap"`:

![](/.gitbook/assets/image-42%20copy.png)

#### Statusrad

Vi avslutar med att infoga en statusrad som berättar skall berätta om appen kunde skicka meddelandet. Vi infogar ett `TextBlock`:

```xml
...
    <Button Margin="5" Height="30">Skicka!</Button>
    <TextBlock Margin="5" Background="#FFF" Foreground="#999">Inga meddelanden...</TextBlock>
</StackPanel>
```

![](/.gitbook/assets/image-43%20copy.png)

#### Fler attribut

Det finns såklart många fler attribut man kan använd för att bygga en gui:

* `FontFamily` mm
* `MinHeight`, `MaxHeight` mm
* `VerticalAlignment`
* `TextWrapping`="WrapWithOverflow" `VerticalScrollBarVisibility`="auto"
* mfl

### Färdiga fönstret

![](/.gitbook/assets/image-44%20copy.png)

#### Mer info

Här kan du läsa mer om:

* Läs mer om `Button`[ http://www.blackwasp.co.uk/WPFButton.aspx](https://github.com/karye/PRRPRR02-TE/tree/204b427e8d7629378601a047369fc260ddb19a13/WPFButton.aspx)
* Läs mer om `TextBox` [http://www.blackwasp.co.uk/WPFTextBox.aspx](http://www.blackwasp.co.uk/WPFTextBox.aspx)
* Läs mer om `StackPanel` [http://www.blackwasp.co.uk/WPFStackPanel.aspx](http://www.blackwasp.co.uk/WPFStackPanel.aspx)
* Läs med om `TextBlock` [http://www.blackwasp.co.uk/TextBlock.aspx](http://www.blackwasp.co.uk/TextBlock.aspx)

### XAML - events och namn

Nu är vi redo att hantera klick på knappen och skicka meddelandet. Vi behöver lägga till ett attribut för knappen:

```xml
<Button Click="KlickSkicka" Margin="5" Height="30">Skicka!</Button>
```

`Click` är ett event som sker när användaren klickar på knappen. 

Textrutorna har ett attribut `Name` som ger dem ett namn. Detta namn kan vi använda för att referera till dem i koden:

```xml
...
    <TextBox Name="rutaEpost" Margin="5" Width="300" />
...
    <TextBox Name="rutaText" Margin="5" Width="300" Height="90" TextWrapping="Wrap" />
...
    <Button Click="KlickSkicka" Margin="5" Height="30">Skicka!</Button>
    <TextBlock Name="rutaStatus" Margin="5" Background="#FFF" Foreground="#999">Inga meddelanden...</TextBlock>
</StackPanel>
```

### MainWindow.xaml.cs

Nu är vi redo att skriva lite kod. Vi skapar en metod som hanterar klick på knappen:

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{

}
```

Vi använder `Text` för att läsa av värdet i textrutan.

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{
    string epost = rutaEpost.Text;
    string text = rutaText.Text;
}
```

Vi lägger till ett villkor som kontrollerar att användaren har skrivit in en epostadress eller inte:

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{
    string epost = rutaEpost.Text;
    string text = rutaText.Text;

    if (epost.Length == 0)
    {
        rutaStatus.Text = "Du måste ange en epostadress!";
    }
    else
    {
        rutaStatus.Text = "Meddelandet skickades!";
    }
}
```

### Skicka mail

Vi börjar med att lägga till en referens till `System.Net.Mail`:

```csharp
using System.Net.Mail;
```

Nu är vi redo att skicka meddelandet. Vi använder `SmtpClient` för att skicka meddelandet:

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{
    // Variabler för epostadress och text
    string id = "epostadress";
    string lösen = "lösenord";

    // Läs av inmatningen
    string epost = rutaEpost.Text;
    string text = rutaText.Text;

    // Kontrollera att användaren har skrivit in en epostadress
    if (epost.Length == 0)
    {
        // Visa felmeddelande
        rutaStatus.Text = "Du måste ange en epostadress!";
    }
    else
    {
        SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
        smtp.EnableSsl = true;
        smtp.Credentials = new NetworkCredential(id, lösen);
        smtp.Send(id, epost, "Meddelande från WPF", text);

        // Visa att meddelandet skickades
        rutaStatus.Text = "Meddelandet skickades!";
    }
}
```

Byt ut `epostadress` och `lösenord` mot din egen epostadress och lösenord.

## Utmaningar

### Kontrollera epostadressen

Mha av reguljära uttryck kan vi kontrollera att användaren har skrivit in en giltig epostadress.  Vi lägger till en referens till `System.Text.RegularExpressions`:

```csharp
using System.Text.RegularExpressions;
```

Nu kan vi kontrollera att användaren har skrivit in en giltig epostadress:

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{
    ...
    // Kontrollera att användaren har skrivit in en epostadress
    string regexEpost = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
    if (!Regex.IsMatch(epost, regexEpost))
    {
        // Visa felmeddelande
        rutaStatus.Text = "Du måste ange en giltig epostadress!";
    }
    else
    {
        ...
    }
}
```

### Felhantering

Vi kan förbättra koden genom att lägga till felhantering. Vi kontrollerar om `SmtpClient` fungerande och om det inte gör det så visas ett felmeddelande.  
Vi lägger till en `try`-`catch`-sats:

```csharp
// Hantera klick på knappen Skicka
private void KlickSkicka(object sender, RoutedEventArgs e)
{
    ...
        try
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            ...
        }
        catch (Exception ex)
        {
            // Visa felmeddelande
            rutaStatus.Text = "Fel: " + ex.Message;
        }
    }
}
```

