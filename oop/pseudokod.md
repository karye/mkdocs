---
description: Uppgifter med pseudokod
---

# Uppgifter med pseudokod

## Kontaktapp

Skapa ett konsolprogram som lagrar och hanterar en kontaktlista med namn, telefonnummer och e-postadresser. Använd `List<>` för att lagra kontakterna och skriv ut dem i konsolen.

* Skapa en klass `Kontakt` med egenskaperna namn, telefonnummer och e-postadress
* Skapa en lista av typen `Kontakt` för att lagra alla kontakter
* Lägg till en metod för att lägga till en ny kontakt till listan, som tar in parametrarna namn, telefonnummer och e-postadress
* Lägg till en metod för att visa alla kontakter i listan
* Lägg till en metod för att söka efter och visa en specifik kontakt i listan, baserat på namn
* Lägg till en metod för att redigera en befintlig kontakt i listan, som tar in parametrarna namn, telefonnummer och e-postadress
* Lägg till en metod för att radera en befintlig kontakt från listan, baserat på namn
* Skapa en loop i huvudprogrammet för att ta emot användarens input och utföra olika metoder, som att lägga till en ny kontakt, visa alla kontakter, söka efter en kontakt, redigera en kontakt eller radera en kontakt
* Lägg till alternativ för att spara och läsa in kontakterna från en textfil för att kunna lagra kontakterna mellan olika körningar av programmet
* Lägg till valmöjligheter för användaren att avsluta programmet eller gå tillbaka till huvudmenyn.
* Upprepa loopen tills användaren väljer att avsluta programmet

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Kontakt {
    + namn : string
    + telefonnummer : string
    + epost : string
    + Kontakt(namn, telefonnummer, epost) : void
    + Sök(namn) : Kontakt
    + Redigera(namn, telefonnummer, epost) : void
    + Radera(namn) : void
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class Kontakt {
        - namn : string
        - telefonnummer : string
        - epost : string
        + Kontakt(namn, telefonnummer, epost) : void
        + Sök(namn) : Kontakt
        + Redigera(namn, telefonnummer, epost) : void
        + Radera(namn) : void
    }
```

## Bokföringsapp

Skapa ett program som hanterar en lista över transaktioner för en företagskassa. Använd `List<>` för att lagra transaktionerna och skriv ut dem i konsolen.

* Skapa en klass `Transaktion` för att lagra transaktionsdata (datum, belopp, typ av transaktion, kommentar etc.)
* Skapa en `List<Transaktion>` för att lagra alla transaktioner
* Skapa en meny i Main för användaren att välja mellan olika alternativ: lägga till en transaktion, visa existerande transaktioner, söka efter en transaktion, redigera eller ta bort en transaktion.
* När användaren väljer att lägga till en transaktion, fråga efter alla nödvändiga transaktionsdetaljer och skapa en ny transaktionsobjekt. Lägg till det i transaktionslistan.
* När användaren väljer att visa transaktioner, gå igenom transaktionslistan och skriv ut alla transaktioner i konsolen.
* När användaren väljer att söka efter en transaktion, fråga efter sökkriterier och gå igenom transaktionslistan för att hitta matchande transaktioner. Skriv ut resultaten i konsolen.
* När användaren väljer att redigera eller ta bort en transaktion, fråga efter transaktions-ID och hitta den i transaktionslistan. Uppdatera eller ta bort transaktionen enligt användarens val.
* Upprepa huvudloopen tills användaren väljer att avsluta programmet.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Transaction {
    + datum : DateTime
    + belopp : decimal
    + typ : string
    + kommentar : string
    + Transaktion(datum, belopp, typ, kommentar) : void
    + Sök(datum, belopp, typ, kommentar) : Transaktion
    + Redigera(datum, belopp, typ, kommentar) : void
    + Radera(datum, belopp, typ, kommentar) : void
}
@enduml
```

## Studentapp

Skapa ett program som hanterar en lista över studenter och deras betyg. Använd `List<>` för att lagra studenterna och skriv ut deras betyg i konsolen.

* Skapa en klass `Student` med egenskaperna namn, betyg, och studentnummer.
* Skapa en `List<Student>` för att lagra alla studenter.
* Lägg till metoder för att lägga till nya studenter till listan, ta bort studenter från listan och uppdatera betyg för en specifik student.
* Lägg till en metod för att sortera studentlistan efter namn eller betyg.
* Lägg till en metod för att skriva ut alla studenter och deras betyg i konsolen.
* I huvudloopen, ge användaren möjlighet att välja mellan olika alternativ såsom att lägga till en student, ta bort en student, uppdatera betyg, sortera listan eller skriva ut listan.
* Hantera input från användaren och kalla på lämpliga metoder för att utföra önskad handling.
* Upprepa huvudloopen tills användaren väljer att avsluta programmet.
* Spara och ladda studentlistan till/från en textfil för att lagra data mellan körningar.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Student {
    + namn : string
    + betyg : int
    + studentnummer : int
    + Student(namn, betyg, studentnummer) : void
    + LäggTill(namn, betyg, studentnummer) : void
    + TaBort(namn, betyg, studentnummer) : void
    + UppdateraBetyg(namn, betyg, studentnummer) : void
    + Sortera(namn, betyg, studentnummer) : void
    + SkrivUt(namn, betyg, studentnummer) : void
}
@enduml
```

## Lagerapp

Skapa ett program som hanterar en lagerlista för en butik. Använd `List<>` för att lagra produkterna och skriv ut dem i konsolen.

* Skapa en klass `Product` med variablerna `name`, `quantity` och `price`.
* Skapa en `List<Product>` kallad `inventory`.
* I Main-metoden:
  * Lägg till exempelprodukter till `inventory`-listan.
  * Skapa en loop för användaren att interagera med programmet.
  * Presentera användaren med alternativ: visa alla produkter i lager, lägg till en produkt, uppdatera en produkts lagersaldo, ta bort en produkt.
  * Beroende på vad användaren väljer, utför motsvarande åtgärd (visa produkter, lägg till ny produkt i listan, uppdatera saldo för en produkt, ta bort produkt från listan).
  * Upprepa loopen tills användaren väljer att avsluta programmet.
  * Spara ändringar i `inventory`-listan till en textfil innan programmet avslutas.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Product {
    + name : string
    + quantity : int
    + price : decimal
    + Product(name, quantity, price) : void
    + ListProducts() : void
    + AddProduct(name, quantity, price) : void
    + UpdateProduct(name, quantity, price) : void
    + RemoveProduct(name, quantity, price) : void
}
@enduml
```

## Födelsedagsapp

Skapa ett program som hanterar en lista över personer och deras födelsedagar. Använd `List<>` för att lagra personerna och skriv ut deras födelsedagar i konsolen.

* Skapa en klass `Person` med variablerna `name`, `birthday`.
* Skapa en `List<Person>` kallad `people`.
* Skapa en loop för användaren att interagera med programmet.
* Skapa en meny i loopen för användaren att välja mellan olika alternativ: lägga till en person, visa existerande personer, söka efter en person, redigera eller ta bort en person.
* Hantera input från användaren och kalla på lämpliga metoder för att utföra önskad handling.
* Upprepa loopen tills användaren väljer att avsluta programmet.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + name : string
    + birthday : DateTime
    + Person(name, birthday) : void
    + AddPerson(name, birthday) : void
    + ListPeople() : void
    + SearchPerson(name, birthday) : void
    + UpdatePerson(name, birthday) : void
    + RemovePerson(name, birthday) : void
}
@enduml
```

## Biljettapp

Skapa ett program som hanterar en lista över biljetter till en konsert. Använd `List<>` för att lagra biljetterna och skriv ut dem i konsolen.

* Skapa en klass `Ticket` med variablerna `name`, `price`.
* Skapa en `List<Ticket>` kallad `tickets`.
* Skapa en loop för användaren att interagera med programmet.
* Skapa en meny i loopen för användaren att välja mellan olika alternativ: lägga till en biljett, visa existerande biljetter, söka efter en biljett, redigera eller ta bort en biljett.
* Hantera input från användaren och kalla på lämpliga metoder för att utföra önskad handling.
* Upprepa loopen tills användaren väljer att avsluta programmet.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Ticket {
    + name : string
    + price : decimal
    + Ticket(name, price) : void
    + AddTicket(name, price) : void
    + ListTickets() : void
    + SearchTicket(name, price) : void
    + UpdateTicket(name, price) : void
    + RemoveTicket(name, price) : void
}
@enduml
```

## Filmapp

Skapa ett program som hanterar en lista över filmer och deras betyg. Använd `List<>` för att lagra filmerna och skriv ut deras betyg i konsolen.

* Skapa en klass `Movie` med variablerna `name`, `rating`.
* Skapa en `List<Movie>` kallad `movies`.
* Skapa en loop för användaren att interagera med programmet.
* Skapa en meny i loopen för användaren att välja mellan olika alternativ: lägga till en film, visa existerande filmer, söka efter en film, redigera eller ta bort en film.
* Hantera input från användaren och kalla på lämpliga metoder för att utföra önskad handling.
* Upprepa loopen tills användaren väljer att avsluta programmet.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Movie {
    + name : string
    + rating : int
    + Movie(name, rating) : void
    + AddMovie(name, rating) : void
    + ListMovies() : void
    + SearchMovie(name, rating) : void
    + UpdateMovie(name, rating) : void
    + RemoveMovie(name, rating) : void
}
@enduml
```