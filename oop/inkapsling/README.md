---
description: Skydda klassens variabler från att ändras av misstag
---

# Inkapsling

## Klassen PersonInkomst

### Publika variabler: public

För att samla data om en persons inkomst har vi skapat en klass som heter `PersonInkomst`.  
Klassen har tre publika variabler:

```csharp
public class PersonInkomst
{
    public string namn;
    public int inkomst;
    public int timmar;
}
```

![](/.gitbook/assets/oop/inkapsling/image-69.png)

I Main-delen kan vi alltså komma åt variablerna direkt:

```csharp
// Skapa ett objekt
PersonInkomst person = new PersonInkomst();

// Läs in data
Console.Write("Ange ditt namn: ");
person.namn = Console.ReadLine();
System.Console.Write("Ange din inkomst: ");
person.inkomst = int.Parse(Console.ReadLine());
Console.Write("Ange antal timmar: ");
person.timmar = int.Parse(Console.ReadLine());
```

### Skydda klassens variabler: private

Att ha publika variabler är inte bra. Det är lätt att ändra på variablerna utan att tänka på vad det kan innebära.  
Vi vill att variablerna ska vara **privata**. Det innebär att de bara kan ändras inom klassen.
Vi byter från `public` till `private`:

```csharp
public class PersonInkomst
{
    private string namn;
    private int inkomst;
    private int timmar;
}
```

![](/.gitbook/assets/oop/inkapsling/image-70.png)

Nu får vi felmeddelande att variablerna `namn`/`inkomst`/`timmar` inte finns.  
Det är för att vi inte kan komma åt dem längre. Vi måste använda **egenskaper** för att komma åt dem.

### Public, private och protected

Inkapsling är ett skydd för en klass så att den inte kan användas på fel sätt.\
Vi kan styra synligheten på följande sätt:

| Synlighet | I klassen | I subklasser | I andra klasser | Symbol |
| --------- | :-------: | :----------: | :-------------: | :----: |
| public    |     ja    |      ja      |        ja       |    +   |
| private   |     ja    |      ja      |       nej       |    -   |
| protected |     ja    |      nej     |       nej       |    #   |

### Videogenomgång

{% embed url="https://youtu.be/_TxOmmJTd98" %}

## Skapa egenskaper i en klass

För att skydda klassens interna variabler gör vi om dessa till metoder med sk `get`- och `set`-metoder.  
Det gör vi genom att skapa egenskaper. Egenskapen är en metod som ser ut som en variabel. 

```csharp
public class PersonInkomst
{
    public string Namn {get; set;}
    public int Inkomst {get; set;}
    public int Timmar {get; set;}
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        - Namn : string «get» «set»
        - Inkomst : int «get» «set»
        - Timmar : int «get» «set»
    }
```

Exempel på hur vi kan använda egenskapen:

```csharp
// Skapa ett objekt
PersonInkomst person = new PersonInkomst();

// Läs in data
Console.Write("Ange ditt namn: ");
person.Namn = Console.ReadLine();
System.Console.Write("Ange din inkomst: ");
person.Inkomst = int.Parse(Console.ReadLine());
Console.Write("Ange antal timmar: ");
person.Timmar = int.Parse(Console.ReadLine());
```

## Konstruktorn

Vi vill också att klassen ska ha en konstruktor som tar emot namn, inkomst och timmar.  
Det gör vi genom att skapa en konstruktor med samma namn som klassen:

```csharp
public class PersonInkomst
{
    private string Namn {get; set;}
    private int Inkomst {get; set;}
    private int Timmar {get; set;}
    
    public PersonInkomst(string namn, int inkomst, int timmar)
    {
        Namn = namn;
        Inkomst = inkomst;
        Timmar = timmar;
    }
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        - Namn : string «get» «set»
        - Inkomst : int «get» «set»
        - Timmar : int «get» «set»
        + PersonInkomst(namn : string, inkomst : int, timmar : int)
    }
```

Main blir då:

```csharp
class Program
{
    static void Main(string[] args)
    {
        // Läs in data
        Console.Write("Ange ditt namn: ");
        string namn = Console.ReadLine();
        System.Console.Write("Ange din inkomst: ");
        int inkomst = int.Parse(Console.ReadLine());
        Console.Write("Ange antal timmar: ");
        int timmar = int.Parse(Console.ReadLine());
        
        // Skapa ett objekt
        PersonInkomst person = new PersonInkomst(namn, inkomst, timmar);
    }
}
```

### Videogenomgång

{% embed url="https://youtu.be/q7aWkjH3UUI" %}
