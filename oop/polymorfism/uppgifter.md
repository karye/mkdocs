# Uppgifter Polymorfism

## Uppgift 17

Implementera en `TillText()`-metod för `Person`-klassen som returnerar personens fullständiga namn (t.ex. "Ada Miller").
Skapa en ny klass som heter Student som ärver från `Person`-klassen och har en ytterligare egenskap som kallas StudentId. Överlagra `TillText()`-metoden i `Student`-klassen så att den också inkluderar studentens ID i utskriften (t.ex. "Ada Miller (ID: 12345)").

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn: string
    + Efternamn: string
    + TillText(): string
}
class Student {
    + StudentId: int
    + TillText(): string
}
Person <|-- Student
@enduml
```

## Uppgift 18

Skapa en annan klass som heter `Lärare` som också ärver från `Person`-klassen och har en ytterligare egenskap som kallas SubjectArea. Överlagra `TillText()`-metoden i `Lärare`-klassen så att den också inkluderar lärarens ämnesområde i utskriften (t.ex. "Ada Miller (Ämne: Matematik)").

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn: string
    + Efternamn: string
    + <<virtual>> TillText(): string
}
class Student {
    + StudentId: int
    + <<override>> TillText(): string
}
class Lärare {
    + SubjectArea: string
    + <<override>> TillText(): string
}
Person <|-- Student
Person <|-- Lärare
@enduml
```

## Uppgift 19

Skapa en metod i `Person`-klassen som heter `Hälsa()` som returnerar en sträng som innehåller en allmän hälsning (t.ex. "Hej, mitt namn är Ada Miller"). Överlagra denna metod i Student- och `Lärare`-klasserna så att studenten säger "Hej, jag är student" och läraren säger "Hej, jag är lärare" respektive.

Skapa en lista med `Person`-objekt och lägg till både studenter och lärare till den. Loopa igenom listan och anropa Speak()-metoden på varje objekt för att se de olika hälsningarna.

Lägg till en virtuell `Beskrivning()`-metod i `Person`-klassen som returnerar en sträng som innehåller personens namn och befattning (t.ex. "Ada Miller är en person"). Överlagra denna metod i Student- och `Lärare`-klasserna så att den returnerar "Ada Miller är student" och "Ada Miller är lärare" respektive.

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn: string
    + Efternamn: string
    + Hälsa(): string
    + <<virtual>> Beskrivning(): string
}
class Student {
    + StudentId: int
    + Hälsa(): string
    + <<override>> Beskrivning(): string
}
class Lärare {
    + SubjectArea: string
    + Hälsa(): string
    + <<override>> Beskrivning(): string
}
Person <|-- Student
Person <|-- Lärare
@enduml
```

## Uppgift 20

Skapa en ny klass som heter `Administratör` som också ärver från `Person`-klassen och har en ytterligare egenskap som kallas Department. Överlagra `Beskrivning()`-metoden i `Administratör`-klassen så att den också inkluderar chefsavdelningen i utskriften (t.ex. "Ada Miller är chef i marknadsavdelningen").

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn: string
    + Efternamn: string
    + Hälsa(): string
    + <<virtual>> Beskrivning(): string
}
class Student {
    + StudentId: int
    + Hälsa(): string
    + <<override>> Beskrivning(): string
}
class Lärare {
    + SubjectArea: string
    + Hälsa(): string
    + <<override>> Beskrivning(): string
}
class Administratör {
    + Department: string
    + Hälsa(): string
    + <<override>> Beskrivning(): string
}
Person <|-- Student
Person <|-- Lärare
Person <|-- Administratör
@enduml
```

## Uppgift 21

Modifiera `Person`-klassen så att den har en skyddad egenskap som heter Age och en skyddad metod som heter `Födelsedag()` som ökar personens ålder med 1. Överlagra `Födelsedag()`-metoden i Student-klassen så att den också skriver ut ett meddelande som säger "Grattis på födelsedagen, [studentnam]" (t.ex. "Grattis på födelsedagen, Ada Miller").

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Person {
    + Förnamn: string
    + Efternamn: string
    - Age: int
    + <<virtual>> Hälsa(): string
    + <<virtual>> Beskrivning(): string
    - <<virtual>> Födelsedag(): void
}
class Student {
    + StudentId: int
    + <<override>> Hälsa(): string
    + <<override>> Beskrivning(): string
    - <<override>> Födelsedag(): void
}
class Lärare {
    + SubjectArea: string
    + <<override>> Hälsa(): string
    + <<override>> Beskrivning(): string
}
class Administratör {
    + Department: string
    + <<override>> Hälsa(): string
    + <<override>> Beskrivning(): string
}
Person <|-- Student
Person <|-- Lärare
Person <|-- Administratör
@enduml
```
