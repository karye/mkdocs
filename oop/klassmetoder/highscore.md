---
description: Skapa en klass som kan återanvändas i flera projekt
---

# Labbar Highscore

![](/.gitbook/assets/oop/klassmetoder/image-117.png)

## Labb 1

### Spara highscore

Skapa grundkoden från mallen:

```csharp
dotnet new console
```

### Klass för spelares poäng

För varje spelare vill vi hålla reda på:

* namn
* poäng
* datum

Vi skapar en klass för detta:

```csharp
class SpelarePoäng
{
    public string namn;
    public int poäng;
    public DateTime datum;
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class SpelarePoäng {
    + namn : string
    + poäng : int
    + datum : DateTime
    }
```

### Spara spelomgångar

Vi kan använda klassen i vårt program:

```csharp
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Spara spelares poäng");

        // Skapa en spelare
        SpelarePoäng spelare1 = new SpelarePoäng();
        spelare1.Namn = "Åke";
        spelare1.Poäng = 100;
        spelare1.Datum = DateTime.Now;
    }
}
```

### Skapa JSON

För att skapa JSON måste klassen justeras lite:

```csharp
using System.Text.Json;
....

// Anpassad till C# System.Text.Json serializer
class SpelarePoäng
{
    public string Namn { get; set; }
    public int Poäng { get; set; }
    public DateTime Datum { get; set; }
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class SpelarePoäng {
    + Namn : string
    + Poäng : int
    + Datum : DateTime
    }
```

### Serialisera objekt -> json

Nu kan vi skapa JSON-text utifrån objekten:

```csharp
// För snyggare indenteringen
var options = new JsonSerializerOptions { 
    WriteIndented = true
};
string json = JsonSerializer.Serialize(spelare1, options);

// Skriv ut JSON
Console.WriteLine(json);

// Spara JSON i fil
File.WriteAllText("highscore.json", json);
```

#### Resultatet i highscore.json

JSON-filen blir nu såhär:

```json
{
  "Namn": "Åke",
  "Poäng": 100,
  "Datum": "2022-11-02T16:16:15.5806742+01:00"
}
```

### Flera spelares poäng

För att spara flera spelares data använder vi oss av en `List\<SpelarePoäng>`:

```csharp
// Skapa en lista för spelarna och deras poäng
List<SpelarePoäng> spelareLista = new List<SpelarePoäng>();

// Skapa en spelare
SpelarePoäng spelare1 = new SpelarePoäng();
spelare1.Namn = "Åke";
spelare1.Poäng = 100;
spelare1.Datum = DateTime.Now;

// Lägg till spelare i listan
spelareLista.Add(spelare1);

// Skapa en ny spelare
SpelarePoäng spelare2 = new SpelarePoäng();
spelare2.Namn = "Lisa";
spelare2.Poäng = 200;
spelare2.Datum = DateTime.Now;

// Lägg till spelare i listan
spelareLista.Add(spelare2);
```

### Serialisera List\<objekt> -> JSON

Nu skickar vi in listan i serialisern:

```csharp
// Spara listan som JSON i fil
string json = JsonSerializer.Serialize(spelareLista, options);

// Skriv ut JSON
Console.WriteLine(json);

// Spara JSON i fil
File.WriteAllText("highscore.json", json);
```

#### Resultatet i highscore.json

JSON-filen ser korrekt ut:

```json
[
  {
    "Namn": "Åke",
    "Poäng": 100,
    "Datum": "2022-11-02T16:16:15.5806742+01:00"
  },
  {
    "Namn": "Lisa",
    "Poäng": 200,
    "Datum": "2022-11-02T16:16:56.5905026+01:00"
  }
]
```

### Läsa in spelare och poäng

Den omvända operationen, dvs omvandla JSON-text till objekt med data görs med `Deserialize`:

```csharp
// Läs in JSON från fil
string json = File.ReadAllText("highscore.json");

// Deserialisera JSON till lista
List<SpelarePoäng> highscoreLista = JsonSerializer.Deserialize<List<SpelarePoäng>>(json);

// Skriv ut listan
foreach (SpelarePoäng spelare in highscoreLista)
{
    Console.WriteLine($"Namn: {spelare.Namn}, poäng: {spelare.Poäng}, datum: {spelare.Datum}");
}
```

## Labb 2

### Metoder för att läsa/skriv JSON

För att få mer ordning på koden skapar vi två metoder:

```csharp
// Metod för att läsa spelarpoäng från JSON-fil
static List<SpelarePoäng> LäsInPoäng()
{
    string filnamn = "highscore.json";
    if (File.Exists(filnamn))
    {
        // Läs in JSON från fil
        string json = File.ReadAllText(filnamn);
        
        // Deserialisera och returnera lista
        return JsonSerializer.Deserialize<List<SpelarePoäng>>(json);
    }
    else
    {
        // Returnera en tom lista
        return new List<SpelarePoäng>();
    }
}

// Metod för att spara spelarpoäng till JSON-fil
static void SparaPoäng(List<SpelarePoäng> spelarePoäng)
{
    // För snyggare JSON
    var options = new JsonSerializerOptions
    {
        WriteIndented = true,
    };

    // Skapa JSON från listan
    string json = JsonSerializer.Serialize(spelarePoäng, options);

    // Spara JSON i fil
    File.WriteAllText("highscore.json", json);
}
```

Med dessa metoder blir Main som följer:

```csharp
// Skapa en lista med spelare
List<SpelarePoäng> spelareLista = new List<SpelarePoäng>();

// Läs in highscore från fil
spelareLista = LäsInSpelarePoäng();

// Skapa en spelare
SpelarePoäng spelare1 = new SpelarePoäng();
spelare1.Namn = "Åke";
spelare1.Poäng = 100;
spelare1.Datum = DateTime.Now;
spelareLista.Add(spelare1);

// Spelets kod
....

// Spara ned spelarens poäng
SparaSpelarePoäng(spelareLista);
```

### Metoderna i klassen

Varför inte låta klassen `SpelarePoäng` sköta metoderna?\
Vi gör så:

```csharp
class SpelarePoäng
{
    public string Namn { get; set; }
    public int Poäng { get; set; }
    public DateTime Datum { get; set; }

    // Metod för att läsa in poäng från JSON-fil
    public List<SpelarePoäng> LäsInPoäng()
    {
        string filnamn = "highscore.json";
        if (File.Exists(filnamn))
        {
            string json = File.ReadAllText(filnamn);
            return JsonSerializer.Deserialize<List<SpelarePoäng>>(json);
        }
        else
        {
            return new List<SpelarePoäng>();
        }
    }

    // Metod för att spara poäng i JSON-fil
    public void SparaPoäng(List<SpelarePoäng> spelarePoäng)
    {
        string json = JsonSerializer.Serialize(spelarePoäng);
        File.WriteAllText("highscore.json", json);
    }
}
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class SpelarePoäng {
    + Namn : string
    + Poäng : int
    + Datum : DateTime
    + LäsInPoäng() List<SpelarePoäng>
    + SparaPoäng(spelarePoäng : List<SpelarePoäng>) void
    }
```

Vår Main blir nu:

```csharp
// Skapa en lista med spelare
List<SpelarePoäng> spelareLista = SpelarePoäng.LäsIn();

// Skapa en spelare
SpelarePoäng spelare1 = new SpelarePoäng();
spelare1.Namn = "Åke";
spelare1.Poäng = 100;
spelare1.Datum = DateTime.Now;
spelareLista.Add(spelare1);

// Spelets kod
....

// Spara ned spelarens poäng
SpelarePoäng.Spara(spelareLista);
```

### En konstruktor

För att ytterligare korta ned koden, kan vi skapa en sk **konstruktor** i klassen:

```csharp
class SpelarePoäng
{
    public string Namn { get; set; }
    public int Poäng { get; set; }
    public DateTime Datum { get; set; }

    // Konstruktor
    public SpelarePoäng(string namn, int poäng, DateTime datum)
    {
        Namn = namn;
        Poäng = poäng;
        Datum = DateTime.Now;
    }
    
    ...
}
```

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class SpelarePoäng {
    +string Namn
    +int Poäng
    +DateTime Datum
    +SpelarePoäng(string namn, int poäng, DateTime datum)
    +List<SpelarePoäng> LäsInPoäng()
    +void SparaPoäng(List<SpelarePoäng> spelarePoäng)
}
@enduml
```
```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class SpelarePoäng {
    + Namn : string
    + Poäng : int
    + Datum : DateTime
    + SpelarePoäng(namn : string, poäng : int, datum : DateTime)
    + LäsInPoäng() List<SpelarePoäng>
    + SparaPoäng(spelarePoäng : List<SpelarePoäng>) void
    }
```


Main blir till slut:

```csharp
// Skapa en lista med spelare
List<SpelarePoäng> spelareLista = SpelarePoäng.LäsIn();

// Skapa en spelare
SpelarePoäng spelare1 = new SpelarePoäng("Åke", 100);
spelareLista.Add(spelare1);

// Spelets kod
....

// Spara ned spelarens poäng
SpelarePoäng.Spara(spelareLista);
```
