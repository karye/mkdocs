---
description: Automagiskt skapa klassdiagram från kod
---

# Klassdiagram med PUML

![](/.gitbook/assets/oop/klasser/image-87.png)

## VS Code tillägg

Installera följande VS Code tillägget:

* [csharp-to-plantuml](https://marketplace.visualstudio.com/items?itemName=pierre3.csharp-to-plantuml)
* [plantuml](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)

### Inställningar

Öppna inställningar (Settings) i VS Code och sök på **plantuml**, välj följande:

* Plantuml: Render -> PlantUMLServer
* Plantuml: Server -> [http://www.plantuml.com/plantuml](http://www.plantuml.com/plantuml)
* Plantuml: Format -> png

![PlantUML inställningar](/.gitbook/assets/oop/klasser/image-88.png)

* csharp2plantuml: All In One -> kryssa i
* csharp2plantuml: Create Association -> kryssa av
* csharp2plantuml: Public -> kryssa av

![C# to PlantUML](/.gitbook/assets/oop/klasser/image-89.png)

## Testa PlantUML

### Ett projekt

Vi utgår från föregående avsnitts kod:

```csharp
using System;

namespace Inkomster
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello klasser");

            // Skapa ett objekt
            PersonInkomst person = new PersonInkomst();

            // Läs in data
            Console.Write("Ange ditt namn: ");
            person.namn = Console.ReadLine();
            System.Console.Write("Ange din inkomst: ");
            person.inkomst = int.Parse(Console.ReadLine());
            Console.Write("Ange antal timmar: ");
            person.timmar = int.Parse(Console.ReadLine());
        }
    }
}
```

### Separat klassfil

Skapa klassen i en separat fil:

```csharp
using System;

namespace Inkomster
{
    public class PersonInkomst
    {
        public string namn;
        public int inkomst;
        public int timmar;
    }
}
```

### Generera UML från koden

Tryck **Ctrl + Shift + P** och välj **C# to PlantUML: Class-diagram**:

![](/.gitbook/assets/oop/klasser/image-90.png)

Automagiskt skapas en katalog **plantuml**:

![](/.gitbook/assets/oop/klasser/image-91.png)

### PersonInkomst.puml

* Öppna _personinkomst.puml_:

```yaml
@startuml
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

* Infoga tre rader för att justera utseendet:

```yaml
@startuml
!theme plain
skinparam classAttributeIconSize 0
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

### Rendera diagram

För att rendera ett diagram:

* Ställ textmarkören någonstans mellan `@startuml` och `@enduml`
* Tryck **Alt+D**

En ny flik öppnas med diagrammet:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

## Uppdatera C#-koden

Låt säg att vi uppdaterar klassen `PersonInkomst` såhär:

```csharp
public class PersonInkomst
{
    private string Namn {get; set;}
    private int Inkomst {get; set;}
    private int Timmar {get; set;}
    
    public PersonInkomst(string namn, int inkomst, int timmar)
    {
        Namn = namn;
        Inkomst = inkomst;
        Timmar = timmar;
    }
}
```

## Standard stil

* Generera UML med **Ctrl + Shift + P** och välj **C# to PlantUML: Class-diagram**
* Infoga raderna i **PersonInkomst.uml**

```
!theme plain
skinparam classAttributeIconSize 0
hide circle
```

* Klassdiagrammet uppdateras automatiskt från:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

* Till detta:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    - Namn : string <<get>> <<set>>
    - Inkomst : int <<get>> <<set>>
    - Timmar : int <<get>> <<set>>
    + PersonInkomst(namn : string, inkomst : int, timmar : int)
}
@enduml
```

## VS Code snippet

Skapa en snippet för puml:

```json
{
	"Class": {
		"prefix": "plantuml-plain",
		"body": [
			"!theme plain",
			"skinparam classAttributeIconSize 0",
            "skinparam defaultFontName \"Roboto\"",
			"hide circle"
		],
		"description": "PlantUML plain theme"
	}
}
```
