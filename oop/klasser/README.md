---
description: Skapa och använda klasser
---

# Skapa en klass

En klass är en mall för att skapa objekt. Man kan likna det med en pepparkaksform som används för att skapa pepparkakor.\
En pepparkaksform är en mall för att skapa pepparkakor. En klass är en mall för att skapa objekt.

![](/.gitbook/assets/oop/klasser/image-80.png)

## Ett enkelt program

### Räkna ut timlön

Vi skapar ett konsolprogram för att räkna ut timlönen för en anställd.\
I programmet ska användaren mata in namn, inkomst och antal arbetade timmar.\
Programmet ska sedan räkna ut timlönen och skriva ut resultatet:

```csharp
using System;

namespace KlasserIntro
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ange ditt namn: ");
            string namn = Console.ReadLine();
            System.Console.Write("Ange din inkomst: ");
            int inkomst = int.Parse(Console.ReadLine());
            Console.Write("Ange antal timmar: ");
            int timmar = int.Parse(Console.ReadLine());

            Console.WriteLine($"Din timlön blev {inkomst / timmar} kr/h");
        }
    }
}
```

Allt väl, men programmet fungerar bara för en person i taget.\
Låt säg att jag är chefen för en avdelning och vill veta vad flera av mina medarbetare har tjänat. Hur gör man då?

## Timlön för flera personer

Hur gör vi för att räkna ut timlönen för flera personer?\
För att lagra flera namn, inkomster och timmar behöver vi någon typ av samling.\
Vi kan använda en array för att lagra flera värden i en variabel.\
Här är ett exempel för tre anställda:

```csharp
using System;

namespace KlasserIntro
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] namn = new string[3];
            int[] inkomst = new int[3];
            int[] timmar = new int[3];

            for (int i = 0; i < namn.Length; i++)
            {
                Console.Write("Ange ditt namn: ");
                namn[i] = Console.ReadLine();
                System.Console.Write("Ange din inkomst: ");
                inkomst[i] = int.Parse(Console.ReadLine());
                Console.Write("Ange antal timmar: ");
                timmar[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < namn.Length; i++)
            {
                Console.WriteLine($"Din timlön blev {inkomst[i] / timmar[i]} kr/h");
            }
        }
    }
}
```

Många variabler blir det här. Det blir svårt att hålla reda på vilken variabel som tillhör vilken person.\
Vi behöver en bättre lösning. Det är dags att skapa en klass.

### Skapa en klass

Vi skapar en klass för att **samla** alla variabler som tillhör en person.\
Klassen ska innehålla namn, inkomst och timmar:

```csharp
public class PersonInkomst
{
    public string namn;
    public int inkomst;
    public int timmar;
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

```mermaid
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'primaryColor': '#FFF',
      'primaryBorderColor': '#000'
    }
  }
}%%
classDiagram
    class PersonInkomst {
        - namn : string
        - inkomst : int
        - timmar : int
        + GetTimlon() int
        + GetTimlon(int skatt) int
    }
```

Vi har nu en klass som innehåller namn, inkomst och timmar.\
Vi kan skapa ett objekt av klassen och använda objektet för att lagra information om en person:

```csharp
using System;

namespace KlasserIntro
{
    public class PersonInkomst
    {
        public string namn;
        public int inkomst;
        public int timmar;
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Skapar objektet person1 från klassen PersonInkomst
            PersonInkomst person1 = new PersonInkomst();
            
            Console.Write("Ange ditt namn: ");
            person1.namn = Console.ReadLine();
            System.Console.Write("Ange din inkomst: ");
            person1.inkomst = int.Parse(Console.ReadLine());
            Console.Write("Ange antal timmar: ");
            person1.timmar = int.Parse(Console.ReadLine());
            
            ...
        }
    }
}
```

Och för nästa person blir det:

```csharp
    // Skapa ett till objekt person2
    PersonInkomst person2 = new PersonInkomst();
    Console.Write("Ange ditt namn: ");
    person2.namn = Console.ReadLine();
    System.Console.Write("Ange din inkomst: ");
    person2.inkomst = int.Parse(Console.ReadLine());
    Console.Write("Ange antal timmar: ");
    person2.timmar = int.Parse(Console.ReadLine());
```

Och för ytterligare en person blir det:

```csharp
    // Skapa ett till objekt person2
    PersonInkomst person3 = new PersonInkomst();
    Console.Write("Ange ditt namn: ");
    person3.namn = Console.ReadLine();
    System.Console.Write("Ange din inkomst: ");
    person3.inkomst = int.Parse(Console.ReadLine());
    Console.Write("Ange antal timmar: ");
    person3.timmar = int.Parse(Console.ReadLine());
```

### Liknelse i verkligheten

Om klassen vore `Car`, kan man föreställa sig det så här:

![](/.gitbook/assets/oop/klasser/image-81.png)

### Videogenomgång

{% embed url="https://youtu.be/ZqDtPFrUonQ" %}

## Klass i separat fil

Klasser läggs i separata filer. Vi kan enklast använda tillägget **C# Toolbox** för detta:

![](/.gitbook/assets/oop/klasser/image-82.png)

Här anger vi namnet på klassen **PersonInkomst.cs**:

![](/.gitbook/assets/oop/klasser/image-83.png)

Koden skapas nu automatiskt:

```csharp
using System;

namespace Timlön7
{
    public class PersonInkomst
    {
        
    }
}
```

Och så fyller vi egenskaperna namn, inkomst och timmar:

```csharp
using System;

namespace Timlön7
{
    public class PersonInkomst
    {
        public string namn;
        public int inkomst;
        public int timmar;
    }
}
```

## Klassdiagram

Klassdiagram representerar grafiskt hur en klass är uppbyggd och förhållandet till andra klasser.

Vår klass ser ut som följer:

```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class PersonInkomst {
    + namn : string
    + inkomst : int
    + timmar : int
}
@enduml
```

```mermaid
classDiagram
    class PersonInkomst {
        - namn : string
        - inkomst : int
        - timmar : int
        + GetTimlon() int
        + GetTimlon(int skatt) int
    }
```

Man kan rita i tex:

{% embed url="https://app.diagrams.net/" %}

{% embed url="https://app.creately.com/" %}

###
