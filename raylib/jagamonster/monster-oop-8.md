---
description: Raylib med OOP
---

# Uppgifter

## Uppgift 1

Här är ett förslag på hur du kan lägga till poängsystem och tidsbegränsning i spelet:

* Lägg till en variabel för poäng och en variabel för tidsbegränsningen i `Main`-metoden, tex:

```csharp
int poäng = 0;
float tidKvar = 30.0f;  // 30 sekunder
```

* Uppdatera poängvariabeln när `hero` kolliderar med `monster` i `Main`-metoden:

```csharp
if (hero.Kolliderar(monster))
{
    poäng++;
}
```

* Räkna ner tiden varje frame i `Main`-metoden och kontrollera om tiden har tagit slut:

```csharp
tidKvar -= Raylib.GetFrameTime();  // Räkna ner tiden varje frame
if (tidKvar <= 0)  // Kontrollera om tiden har tagit slut
{
    Raylib.CloseWindow();  // Stäng fönstret för att avsluta spelet
}
```

* Rita ut poängen och tiden kvar på skärmen:

```csharp
Raylib.DrawText(string.Format("Poäng: {0}", poäng), 10, 10, 20, Color.WHITE);
Raylib.DrawText(string.Format("Tid kvar: {0:0.0} sekunder", tidKvar), 10, 30, 20, Color.WHITE);
```

Detta är bara ett förslag på hur du kan implementera poängsystemet och tidsbegränsningen i spelet, och det finns många olika sätt att göra det på. Du kan tex också lägga till en poängräknare som en separat klass och uppdatera poängen och tiden där istället för i `Main`-metoden.

## Uppgift 2

Lägg till en meny för att låta spelaren välja olika svårighetsgrader och inställningar, tex val av figurer och hinder som ska finnas i spelet. Du kan också lägga till en högstapointabell för att visa de högsta poängen som någonsin har uppnåtts i spelet.

* Skapa en menyskärm som en separat klass, tex `Meny`-klassen. I den här klassen kan du lägga till olika knappar och rutor för att låta spelaren välja olika alternativ. Du kan också lägga till en textruta där du kan visa olika meddelanden till spelaren, tex instruktioner eller felmeddelanden.

* Skapa olika metoder i `Meny`-klassen för att hantera olika händelser, tex när spelaren trycker på en knapp eller väljer ett alternativ i menyn. Du kan också lägga till en metod för att rita ut menyskärmen på skärmen.

* Lägg till en menyvariabel i Program-klassen, tex:

```csharp
Meny meny = new Meny();
```

* I Main-metoden, lägg till en if-sats som kontrollerar om menyn ska visas eller inte:

```csharp
if (meny.VisaMeny) // Visa menyn om den ska visas
{
    meny.RitaUtMeny();
}
else // Annars, visa spelet
{
    // Spelet körs här
}
```

* I Meny-klassen, lägg till en metod för att hantera spelarens val i menyn, tex:

```csharp
public void HanteraVal()
{
    if (Raylib.IsKeyPressed(KeyboardKey.KEY_ENTER))
    {
        // Spelaren har tryckt på Enter-knappen
        // Här kan du lägga till kod för att hantera spelarens val
    }
}
```

## Fler uppgifter

Här är några förslag på förbättringar du kan göra på spelet:

* Lägg till fler figurer och hinder för att göra spelet mer utmanande och roligt. Du kan t ex lägga till fler monster av olika slag, och låta spelaren vinna olika belöningar (t ex poängbonusar och liv) genom att fånga dessa.

* Lägg till olika nivåer i spelet, där varje nivå har sin egen bakgrund, figurer och hinder. Du kan låta spelaren välja vilken nivå som ska spelas genom att använda menyn.

* Lägg till olika power-ups och föremål som spelaren kan samla på sig under spelets gång, t ex föremål som ger spelaren bonuspoäng, extra liv, snabbare hastighet eller en temporär förmåga att skada monster.

* Lägg till en "game over"-skärm som visas när spelaren förlorar alla sina liv, och låt spelaren välja om den vill spela igen eller avsluta spelet från denna skärm.

Detta är bara några förslag, och det finns många fler förbättringar du kan göra på spelet beroende på vad du är intresserad av och vad du vill uppnå med spelet.