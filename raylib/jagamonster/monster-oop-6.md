---
description: Raylib med OOP
---

# Styra hjälten med piltangenterna

Nu är det dags att lägga till lite interaktion med användaren. Användaren ska kunna styra hjälten i spelet med piltangenterna. Vi gör det genom att kontrollera om användaren trycker på en piltangent. Om användaren trycker på en piltangent ändrar vi på hjältens position.

## Klassen Hjälte

Vi skapar en ny klass som heter `Hjälte`. 

```csharp
// Klassen Hjälte
public class Hjälte
{
    private Texture2D _textur;
    private Rectangle _rektangel;

    // Konstruktor
    public Hjälte(int x, int y, string texturFil)
    {
        _textur = Raylib.LoadTexture(texturFil);
        _rektangel = new Rectangle(x, y, _textur.width, _textur.height);
    }

    // Metod för att rita ut hjälten
    public void Rita()
    {
        Raylib.DrawTexture(_textur, (int)_rektangel.x, (int)_rektangel.y, Color.WHITE);
    }

    // Metod för att uppdatera hjälten
    public void Uppdatera()
    {
        // Kontrollera om användaren trycker på en piltangent
        if (Raylib.IsKeyDown(KeyboardKey.KEY_RIGHT))
        {
            // Flytta hjälten till höger
            _rektangel.x += 1;
        }
        else if (Raylib.IsKeyDown(KeyboardKey.KEY_LEFT))
        {
            // Flytta hjälten till vänster
            _rektangel.x -= 1;
        }
        else if (Raylib.IsKeyDown(KeyboardKey.KEY_UP))
        {
            // Flytta hjälten uppåt
            _rektangel.y -= 1;
        }
        else if (Raylib.IsKeyDown(KeyboardKey.KEY_DOWN))
        {
            // Flytta hjälten nedåt
            _rektangel.y += 1;
        }
    }
}
```


```puml
@startuml
!theme plain
skinparam classAttributeIconSize 0
skinparam defaultFontName "Roboto"
hide circle
class Hjälte {
    -_textur : Texture2D
    -_rektangel : Rectangle
    +Hjälte(int x, int y, string texturFil)
    +Rita()
    +Uppdatera()
}
@enduml
```

## Main

I `Main` använder vi nu klassen `Hjälte` istället för `Figur`.

```csharp
...

// Skapa en figur
Hjälte hero = new Hjälte(100, 100, "./resurser/hero.png");

while (!Raylib.WindowShouldClose())
{
    ...

    Raylib.BeginDrawing();
    Raylib.ClearBackground(Color.DARKGREEN);

    ...

    // Rita ut figuren
    hero.Rita();

    Raylib.EndDrawing();
}
```

Men vi får nu fel i koden eftersom vi saknar metoden `Kolliderar()` i klassen `Hjälte`. 

## Kolisionsmetoden

Vi lägger till en metod som heter `Kolliderar()` i klassen `Hjälte`. Metoden tar emot en `Rectangle` som parameter. Metoden returnerar `true` om hjälten kolliderar med den angivna rektangeln, annars returneras `false`.

### Klassen Hjälte

```csharp
// Metod för att kontrollera om hjälten kolliderar med en figur
public bool Kolliderar(Figur figur)
{
    // Om hjälten inte kolliderar med figuren, returnera false
    if (!Raylib.CheckCollisionRecs(_rektangel, figur.Rektangel()))
    {
        return false;
    }

    // Om vi har kommit hit så krockar de, returnera true
    return true;
}
```

![](/.gitbook/assets/raylib/jagamonster/JagaMonster-diagram-7.png)

### Klassen Figur

En ändring måste vi göra i klassen `Figur` också. Vi måste lägga till en metod som heter `Rektangel()` som returnerar figurens rektangel. 

```csharp
// Metod för att returnera figurens rektangel
public Rectangle Rektangel()
{
    return _hitbox;
}
```

![](/.gitbook/assets/raylib/jagamonster/JagaMonster-diagram-8.png)

## Förbättringar

### Förbättring 1

Se till att hjälten inte kan gå utanför skärmen. Ej heller utanför skogsbrynet.

### Förbättring 2

Om hjälten träffar monstret ska monstret försvinna. Vi gör det genom att lägga till en `if`-sats i `Main` som kollar om hjälten kolliderar med monstret. Om det är så ska monstret dö och spawna ett nytt monstret.

### Förbättring 3

Lägg till mynt som hjälten kan plocka upp. Om hjälten plockar upp ett mynt ska myntet försvinna och spelaren ska få poäng.