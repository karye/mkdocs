---
description: Raylib med OOP
---

# Spelet Shmup

## Resultat

![Arcade spel](/.gitbook/assets/raylib/shmup/arcade-game.png)

## Beskrivning

Shmup betyder "Shoot 'em up" och är ett arkadspel där spelaren kontrollerar ett skepp som skjuter mot fiender. Det finns också en bakgrundsbild och en poängräknare (Scen).

Spelet använder **Raylib_cs** för grafik och inputhantering och använder **OOP** för att organisera koden.

Spelet kan ha med följande:

* Spelaren styr skeppet med piltangenterna
* Skeppet skjuter med mellanslag
* Skeppet kan inte åka utanför scenen
* Visa Liv och Poäng på skärmen
* Olika fiender som kommer inflygande uppifrån
* Power-ups som ger extra liv
* Power-ups som ger extra skott
* Lägga till bakgrundsmusik
* Lägga till ljud när skeppet skjuter och kolliderar med fiender
