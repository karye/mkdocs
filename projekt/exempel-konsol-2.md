---
description: Exempel på konsoleapplikationer
---

# Exempel på konsoleapplikationer

## Konsoleapplikationer med två klasser

#### Kontaktuppgifter

En konsolapplikation för att hålla koll på **kontaktuppgifter**, där användaren kan lägga till och ta bort kontakter samt söka efter en viss kontakt baserat på namn eller telefonnummer.

Pseudokod för programmet:

* Skapa en lista för att lagra kontaktinformation (namn, telefonnummer).
* Visa meny med alternativ: lägg till kontakt, ta bort kontakt, sök kontakt, visa alla kontakter, avsluta programmet.
* Läs in användarens val från menyn.
* Om användaren väljer "lägg till kontakt":
  * Fråga efter kontaktens namn och telefonnummer.
  * Lägg till kontakten i listan.
  * Gå tillbaka till menyn.
* Om användaren väljer "ta bort kontakt":
  * Fråga efter namn eller telefonnummer för kontakten som ska tas bort.
  * Sök efter kontakten i listan.
* Om kontakten hittas, ta bort den från listan.
  * Gå tillbaka till menyn.
* Om användaren väljer "sök kontakt":
  * Fråga efter namn eller telefonnummer för kontakten som ska sökas.
  * Sök efter kontakten i listan.
* Om kontakten hittas, visa kontaktens information.
  * Gå tillbaka till menyn.
* Om användaren väljer "visa alla kontakter":
  * Visa alla kontakter i listan.
  * Gå tillbaka till menyn.
* Om användaren väljer "avsluta programmet":
  * Avsluta programmet.

Här är några förslag på hur man kan utöka programmet:

* Möjlighet att redigera en befintlig kontakt.
* Möjlighet att sortera kontakterna i listan på olika sätt (t.ex. efter namn, telefonnummer).
* Möjlighet att lägga till flera telefonnummer eller e-postadresser för en kontakt.
* Möjlighet att spara och läsa in kontakterna från en extern textfil.
* Validering av användarens inmatning (t.ex. kontrollera att det bara är siffror som matas in för telefonnummer).
* Möjlighet att söka kontakt med delar av namn eller telefonnummer.
* Möjlighet att skriva ut en rapport med alla kontakter.

#### Bokhylla

En konsolapplikation för att hålla koll på **böcker man läst**, där användaren kan lägga till och ta bort böcker från sin lista, samt visa information om en viss bok (titel, författare, antal sidor etc) från en textfil.

#### Trädgård

En konsolapplikation för att hålla koll på sin **trädgård**, där användaren kan lägga till och ta bort växter från sin lista och visa information om en viss växt (namn, planteringsdatum, skötselråd etc) från en textfil.
