# SR-appen - WPF

_Förkunskaper är klasser, XML/JSON och WPF._

## Uppgift

Upplägget är att du ska skapa ett program som visar tablån för en kanal.  
Du ska kunna välja kanal och sedan visa tablån för den kanalen.  
Du ska även kunna välja vilken dag som tablån ska visas för.  
Det ska finnas en knapp som gör att tablån uppdateras.

![](/.gitbook/assets/projekt/image-134.png)

## Datakälla

Du ska hämta data från [SR API](https://sverigesradio.se/api/documentation/v2/index.html).  
Datat är i XML format. Du kan använda dig av [XML-serialisering](https://csharp.progdocs.se/csharp-ref/filhantering/serialisering-.../xml-serialisering) för att läsa in datat.

### Exempel på anrop
Exempelsvar (förkortat):
    
```xml
<sr>
  <copyright>Copyright Sveriges Radio 2012. All rights reserved.</copyright>
  ...
  <schedule>
    ...
    <scheduledepisode>
      <episodeid>128761</episodeid>
      <title>P3 Musik</title>
      <starttimeutc>2012-09-19T04:05:00Z</starttimeutc>
      <endtimeutc>2012-09-19T04:30:00Z</endtimeutc>
      <program id="4323" name="P3 Musik" />
      <channel id="164" name="P3" />
      <imageurl>http://sverigesradio.se/images/4835/3650649_2048_1152.jpg?preset=api-default-square</imageurl>
      <imageurltemplate>http://sverigesradio.se/images/4835/3650649_2048_1152.jpg</imageurltemplate>
    </scheduledepisode>
    ...
  </schedule>
</sr>
```

### Exempel på kod

```csharp
using System;
using System.Xml.Serialization;
using System.Net;
using System.IO;
using System.Collections.Generic;

namespace srAppen
{
    [XmlRoot("sr")]
    public class Sr
    {
        public string copyright { get; set; }
        public RadioPagination pagination { get; set; }
        public RadioSchedule schedule { get; set; }
    }
    [XmlRoot("pagination")]
    public class RadioPagination
    {
        public int page { get; set; }
        public int pagesize { get; set; }
        public int totalhits { get; set; }
        public int totalpages { get; set; }
    }
    [XmlRoot("schedule")]
    public class RadioSchedule
    {
        [XmlElement("scheduledepisode")]
        public List<RadioScheduleEpisode> scheduleEpisode { get; set; }
    }
    [XmlRoot("scheduledepisode")]
    public class RadioScheduleEpisode
    {
        public int episodeid { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string starttimeutc { get; set; }
        public string endtimeutc { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Hämta data från SR API
            var url = "http://api.sr.se/api/v2/scheduledepisodes?channelid=164";

            // Skapa en webclient
            WebClient client = new WebClient();

            // Hämta XML
            string xml = client.DownloadString(url);

            // Hur XMl ska tolkas
            XmlSerializer serializer = new XmlSerializer(typeof(Sr));

            // Deserialisera XML
            Sr sr = (Sr)serializer.Deserialize(new StringReader(xml));

            // Kolla om svaret är tomt
            if (sr == null)
            {
                Console.WriteLine("Inga program hittades");
                return;
            }

            // Skriv ut copyright
            Console.WriteLine(sr.copyright);

            // Skriv ut pagination
            Console.WriteLine($"Sida {sr.pagination.page} av {sr.pagination.totalpages}");

            // Skriv ut program
            foreach (var episode in sr.schedule.scheduleEpisode)
            {
                Console.WriteLine($"Program: {episode.title}");
                
            }
        }
    }
}
```

## Kravlista

Kraven är listade i prioriteringsordning:  

* [ ] Du ska kunna välja kanal.
* [ ] Du ska visa vilket program som sänds just nu.
* [ ] Du ska visa mer information om programmet.
* [ ] Du ska kunna välja vilken dag som tablån ska visas för.
* [ ] Du ska kunna se tablån för vald kanal och vald dag.
* [ ] Du ska kunna navigera mellan dagarna.

## Tips

{% embed url="https://youtu.be/ulzFqXb13dI" %}
