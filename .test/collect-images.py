#!/usr/bin/python3

import os
import re
import json

# Constants
# Define the source directory for the markdown files
md_dir = "."

# Define the directory for the images
image_dir = "images/"

# Define the output file for the JSON data
output_file = "images.json"

# Regular expression to match image paths in markdown files
# This pattern searches for a string that starts with an exclamation mark,
# followed by square brackets, followed by a pair of parentheses
# that contain the image path
image_pattern = re.compile(r"!\[.*\]\((.*)\)")

# Dictionary to store the collected image paths
images = {}

# Function to create the folder structure for the image
def create_folder_structure(images, image_dir):
    for image_path in images.values():
        # Create the folder structure for the image if it doesn't already exist
        os.makedirs(os.path.dirname(os.path.join(image_dir, image_path)), exist_ok=True)


# Iterate through all markdown files in the source directory
for root, dirs, files in os.walk(md_dir):
    # Skip hidden dot-folders and "docs" folder
    dirs[:] = [d for d in dirs if not d.startswith(".") and d != "docs"]
    print(f"Processing directory: {root}")

    for file in files:
        # Skip files that are not markdown files
        if not file.endswith(".md"):
            continue

        # Print the name of the file being parsed
        print(f"Parsing file: {file}")

        # Construct the full path for the file
        file_path = os.path.join(root, file)

        # Read the contents of the file
        with open(file_path, "r") as f:
            contents = f.read()

        # Find all image paths in the file
        for match in image_pattern.finditer(contents):
            image_path = match.group(1)

            # Skip external images
            # An image is considered external if it starts with "http" or "www"
            if "http" in image_path or "www" in image_path:
                continue

            # Construct the full path to the image
            # os.path.relpath returns the relative path of image_path with respect to md_dir
            image_path_rel = os.path.join(image_dir, os.path.relpath(image_path, md_dir))

            # Print the image path
            print(f"Found image: {image_path}")

            # Store the image path in the dictionary
            images[image_path] = image_path_rel

# Create the folder structure for the images
create_folder_structure(images, image_dir)

# Write the collected image paths to a JSON file
with open(output_file, "w") as f:
    # json.dump takes the data and writes it to the file in JSON format
    # The indent argument is set to 2 to make the output more readable
    json.dump(images, f, indent=2)

# Print a message to indicate that the process is complete
print("Done!")