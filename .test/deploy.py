#!/usr/bin/python3

import os
import shutil

origin = 'mkdocs/'
sites = ['graegrae/', 'prrprr01-js/', 'prrprr01-te/', 'prrprr02-te/', 'tillprr/',  'wesweb01/', 'wesweb02/', 'weuweb01/', 'weuweb02/']
#sites = ['test/']

for site in sites:
    # Update mkdocs.yml
    filelist = [site + 'head.yml', origin + 'base.yml']
    with open(site + 'mkdocs.yml', 'w') as outfile:
        for file in filelist:
            try:
                with open(file) as infile:
                    outfile.write(infile.read())
                print('Merged change to ' + site + 'mkdocs.yml')
            except:
                print('Missing ' + file)

    # Update scripts
    shutil.rmtree(site + 'scripts/', ignore_errors=True)
    os.mkdir(site + 'scripts/')
    shutil.copytree(origin + 'scripts', site + 'scripts/', dirs_exist_ok=True)
    print('Updated: ' + origin + 'scripts/')

    # Update css
    shutil.rmtree(site + 'style/', ignore_errors=True)
    os.mkdir(site + 'style/')
    shutil.copytree(origin + 'style', site + 'style/', dirs_exist_ok=True)
    print('Updated: ' + origin + 'style/')

    # Update extra files
    filelist = ['gitbook2mkdocs.py']
    for file in filelist:
        shutil.copy(origin + file, site + file)
        print('Updated: ' + site + file)